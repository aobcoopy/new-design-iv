<div class="container-fluid yachthing_box">
	<div class="row justify-content-center text-center">
    	<div class="col-12 col-md-10">
            <div class="yacht_tt hid animate__delay-05s">our yachting <br class="mob">and excursion <br class="mob">collections</div>
        </div>
        <div class="col-12 col-md-7">
            <div class="yacht_des animate__animated hid animate__delay-09s animate__fast">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet
    adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet</div>
        </div>
    	
    </div>
    
    <div class="row justify-content-center ya1 ">
    	<div class="col-12 col-lg-9 ">
            <div class="row">
                <div class="col-6 nopad web"><img data-src="../../upload/new_design/img-Iy-Ie-img.png" class="yacht_img lazy yal hid animate__delay-09s" alt=""></div>
                <div class="col-6 nopad web yar hid animate__delay-09s">
                    <div class="row- y_box ">
                        <div class="yacht_in_tt">FIND YOUR IDEAL BEACHVILLA IN THAILAND TODAY</div>
                        <div class="yacht_in_des">Finding the right luxury villa for rent in Thailand
            is key to creating the kind of experience that will
            be truly unforgettable. Browse our collection of
            luxury villas online today to find the perf ect
            property for your dream Thailand holiday.</div>
                        <a href=""><button class="yacht_but">visit</button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center ex1">
    	<div class="col-12 col-lg-9 ">
            <div class="row">
                <div class="col-6 nopad web exl hid animate__delay-09s">
                    <div class="row- y_box">
                        <div class="yacht_in_tt">FIND YOUR IDEAL BEACHVILLA IN THAILAND TODAY</div>
                        <div class="yacht_in_des">Finding the right luxury villa for rent in Thailand
            is key to creating the kind of experience that will
            be truly unforgettable. Browse our collection of
            luxury villas online today to find the perf ect
            property for your dream Thailand holiday.</div>
                        <a href=""><button class="yacht_but pp">visit</button></a>
                    </div>
                </div>
                <div class="col-6 nopad web"><img data-src="../../upload/new_design/img-Iy-Ie-img2.png" class="yacht_img lazy exr hid animate__delay-09s" alt=""></div>
            </div>
        </div>
    </div>
    
    
    <div class="row justify-content-center mob">
    	<div class="col-12 nopad"><img data-src="../../upload/new_design/img-Iy-Ie-img.png" class="yacht_img lazy" alt=""></div>
        <div class="col-12 nopad">
        	<div class="row- y_box">
                <div class="yacht_in_tt">FIND YOUR IDEAL BEACHVILLA IN THAILAND TODAY</div>
                <div class="yacht_in_des">Finding the right luxury villa for rent in Thailand
    is key to creating the kind of experience that will
    be truly unforgettable. Browse our collection of
    luxury villas online today to find the perf ect
    property for your dream Thailand holiday.</div>
                <a href=""><button class="yacht_but">visit</button></a>
            </div>
        </div>
    </div>
    <div class="row justify-content-center mob">
    	<div class="col-12 nopad"><img data-src="../../upload/new_design/img-Iy-Ie-img2.png" class="yacht_img lazy" alt=""></div>
        <div class="col-12 nopad">
        	<div class="row- y_box">
                <div class="yacht_in_tt">FIND YOUR IDEAL BEACHVILLA IN THAILAND TODAY</div>
                <div class="yacht_in_des">Finding the right luxury villa for rent in Thailand
    is key to creating the kind of experience that will
    be truly unforgettable. Browse our collection of
    luxury villas online today to find the perf ect
    property for your dream Thailand holiday.</div>
                <a href=""><button class="yacht_but pp">visit</button></a>
            </div>
        </div>
        
    </div>
    
    
</div>

